#ifndef CAM_H
#define CAM_H
#include <QObject>
#include <QApplication>
#include <QTime>
#include <QThread>
#include <QDebug>
#include <include/lslib_global.h>

#define IMAGE_SZ 5029780

class cam : public QThread
{
    Q_OBJECT
public:
    cam(lsdev *h):
        dev(h),
        capture(0)
    {
        fr_speed = 0;
        return;
    }
    quint32 fr_speed;
    void set_capture(quint32 set)
    {
        capture = set;
    }


    void stop()
    {
        capture = false;
        running = false;
        return;
    }


    void run() Q_DECL_OVERRIDE
    {
        int res = 0;
        running = true;
        struct fr t;
        t.buf = NULL;
//        t.buf = new quint8[IMAGE_SZ*2];
        while (running)
        {
            while (!capture && running) msleep(1000);

//            if (dev->type == LS_A2)
//                lslib_flowctl(dev, 1);

            while(capture)
            {

                QTime myTimer;
                myTimer.start();
           //
               t.buf = new quint8[dev->frame.size*2];
                if (dev->type == LS_A2)
                    res = lslib_flow(dev,(unsigned char *)t.buf);

                if (dev->type == LS_A1)
                    res = lslib_getImage(dev,(unsigned char *)t.buf);

                if (res) {
                    delete t.buf;
                    continue;
                }
                fr_speed = myTimer.elapsed();
                fr_list.push_back(t);

            }
         //   lslib_flowctl(dev, 0);
        }
        qDebug() << "Exit cam_thr";
    }


    struct fr
    {
        quint8 * buf;
    };

    QList <fr> fr_list;

    QList <cam::fr> * get_image_list()  {
        return &fr_list;
    }
private:
    bool running;
    lsdev *dev;
    bool capture;
signals:
    void hasImage();
    void finished();
};


#endif // CAM_H
