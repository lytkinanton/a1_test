/*!
 * \file glimage.h
 * \brief OpenGL image draw class definitions
 * \author Andrey Valik
 * \date   created 22-Feb-2011
 */

#ifndef GLDEPTH_H
#define GLDEPTH_H

#include <QtGui>
#include <QGLWidget>

/**
 * OpenGL image class
 */
class GLImage: public QGLWidget
{
    Q_OBJECT

    public:
    /**Constructs the class object.
     *
     * @param parent    - parent object
     * @param flags     - object flags
     */
    GLImage( QWidget *parent, Qt::WindowFlags flags );

    /**Destroys the class object*/
    ~GLImage();

    /**Draws RGB image
     *
     * @param image         - pointer to the image buffer, image format is RGB, 24bpp.
     * @param img_width     - number of pixels per image row
     * @param img_height    - number of rows per image
     */
    void drawImage( unsigned char* image, int width, int height );

    /**Draws gray-scaled image
     *
     * @param image         - pointer to the image buffer, image format is grayscaled, 8bpp.
     * @param img_width     - number of pixels per image row
     * @param img_height    - number of rows per image
     */
    void drawImageGray( unsigned char* image, int width, int height );

    /** Returns version of library
     *
     * \return version of library
     * */
    const char* version();

    protected:

    /**Paint event handler*/
    virtual void paintEvent ( QPaintEvent * event );

    /**Releases the textures, that allocated into GLImage::initTextures()  */
    void deleteTextures();

    /** Returns default widget size
     *
     * @return  default widget size
     */
    QSize sizeHint();

    /**Returns default minimum widgets size
     *
     * @return default minimum widgets size
     */
    QSize minimumSizeHint();

    /** Resizes the GL scene.
     *  Called by QT when widget resized.
     *
     * @param width     - new widget width
     * @param height    - new widget height
     */
    void resizeGL( int width, int height );

    /** Repaint GL parts of widget.
     *
     *  Called by QT when widget repainted
     */
    void paintGL();

    /**Open GL part initialization, called by QT. */
    void initializeGL();

    /**Send GRB image to GPU and attach to texture
     *
     * @param image         - pointer to the GRB image buffer, 24bpp.
     * @param img_width     - number of pixels per row
     * @param img_height    - number of rows per image
     */
    void setImage2Texture( unsigned char* image, int img_width,
            int img_height );

    /**Send gray-scaled image to GPU and attach to texture
     *
     * @param image         - pointer to the gray-scaled image buffer, 8bpp.
     * @param img_width     - number of pixels per row
     * @param img_height    - number of rows per image
     */
    void setImageGray2Texture( unsigned char* image, int img_width,
            int img_height );

    /**Pointer to the glXWaitVideoSyncSGI function*/
    void (*videoSyncWait)( int, int, unsigned int* );

    /**Pointer to the glXGetVideoSyncSGI function*/
    void (*videoSyncGet)( unsigned int* );

    /** Waits passed number of vertical syncs.
     *
     * Functions waits first vertical sync and swap GL buffers,
     * and waits passed number of syncs again.
     *
     * @param syncs - number of sync to wait, as usual syncs = 0.
     */
    void waitSync( int syncs = 0 );

    /*Draws OpenGL scene*/
    void drawNowImage();

    /**Generates textures and fills parameters*/
    void initTextures();

    /**Correct widget geometry to store aspect ration*/
    void correctSize(QSize* size);

    /**Count texture size, must be power of 2*/
    void countTextureSize();

    private:

    /**Number of pixels per image row - image width*/
    int imageWidth;

    /**Number of rows per image - image height*/
    int imageHeight;

    /**Texture image width, must be power of 2*/
    int textureWidth;

    /**Texture image height, must be power of 2*/
    int textureHeight;

    /**RGB image texture object*/
    GLuint iImageTexture;

    /**Gray-scaled image texture object*/
    GLuint iImageGrayTexture;

    /**Draw gray-scaled image flag.
     * - if TRUE then draw gray-scaled image
     * - if FALSE then draw RGB image
     */
    bool iDrawGray;

    /**RGB texture was allocated by glTexImage2D()*/
    bool iCreated;

    /**GRAY texture was allocated by glTexImage2D()*/
    bool iCreatedGray;

    /*Object was created and painted*/
    bool iInited;

    signals:
    void doneSignal();

};

#endif
