#ifndef MW_H
#define MW_H
#include <QTimer>
#include <QMainWindow>
#include <QProgressBar>
#include <QProgressDialog>
#include <QResizeEvent>
#include <QThread>
#include <queue>
#include <cam.h>

namespace Ui {
class mw;
}

class mw : public QMainWindow
{
    Q_OBJECT
    cam *hcam;
public:
    void run_camera_capture();
    void stop_camera_capture();

    explicit mw(QWidget *parent = 0);
    ~mw();
private slots:
    void usb_update();

    void bot_l_sl_sliderMoved(int position);

    void top_l_sl_sliderMoved(int position);

    void gain_sl_sliderMoved(int position);

    void offset_sl_sliderMoved(int position);

    void on_Getframe_released();

    void on_openclose_bt_released();

    void on_test_rom_bt_released();

    void on_fw_read_bt_released();

    void on_fw_write_bt_released();

    void on_bot_r_sl_sliderMoved(int position);

    void on_top_r_sl_sliderMoved(int position);
    void mwexit();
    void on_usblist_refresh_bt_released();


    void on_i2c_read_bt_released();

    void on_i2c_write_bt_released();

    void on_soi_init_bt_released();

    void on_wrom_bt_released();

    void on_clear_rom_bt_released();

    void on_wram_bt_released();

    void on_mt_i2c_read_bt_released();

    void on_mt_i2c_write_bt_released();

    void on_en_vd_cb_toggled(bool checked);

    void on_trig_cb_stateChanged(int arg1);
    void on_expo_sl_sliderMoved(int position);
    void on_save_img_bt_released();
    void on_en_autoexp_cb_toggled(bool checked);

    void on_ac_fw_width_eb_textChanged(const QString &arg1);

    void on_extra_data_eb_textChanged(const QString &arg1);

    void on_rst_sens_bt_released();

    void on_test_rom_bt_2_released();

    void on_zoom_cb_toggled(bool checked);

    void on_zoom_sl_valueChanged(int value);

public slots:
    void get_fr_update();
private:
    static void setPercentOfComplete(int progress);
    lsdev *current_dev;
    QImage frame_image;
    QThread *camthread;
    Ui::mw *ui;
    QImage *m_image;
    QPixmap origin_px;
    QTimer get_fr;
    QTimer usb_list_timer;
    quint32 frame_n;
    quint32 err_frame_n;
    QImage zi;
    static QProgressDialog* progressDialog;
signals:
    void stopAll();
};

#endif // MW_H
