#ifndef FRAMER_H
#define FRAMER_H

#include <QWidget>
#include <QLabel>
#include <QPainter>

//class framer : public QOpenGLWidget
//{


//    Q_OBJECT
//    QImage image;
//public:
//    framer(QWidget *parent) : QOpenGLWidget(parent)
//    {}

//    void display(const QImage& img);
//    void paintGL() override;
//};

class framer : public QLabel
{
    Q_OBJECT
public:
    framer(QWidget *parent) : QLabel(parent)
    {}

    void display(const QImage& img);
};


#endif // FRAMER_H
