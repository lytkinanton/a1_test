 #include <qdebug.h>
#include <QTime>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include <libusb.h>
#include <include/lslib_global.h>
#include "ls.h"
//static lsdev cydev[MAXDEVICES];

// I2C addresses
#define AR0131_SENS_ADDR 0x1003 // 5d02
#define MT9_SENS_ADDR 0x9102
#define J003_SENS_ADDR 0x2103
#define SOI968_SENS_ADDR 0x3002

#define EERPOM_ADDR 0x5101

static int nid;		/* Number of Interesting Devices	*/

struct VPD {
    unsigned short	vid;
    unsigned short	pid;
    char		desc[MAX_STR_LEN];
};

static struct VPD vpd[] = {
        { 0x4b4, 0x8001, "LS1 Fingerprint scanner"},
        { 0x4b4, 0x8002, "LS8 Fingerprint scanner"},
        { 0x4b4, 0x8613, "LS unprogrammed unit"}};

static int sensor_enable_offset(lslib_handle *h);
static int sensor_control (lslib_handle *h, unsigned short reg, unsigned char *buf, int write, unsigned short sensorAddr);
static int req_frame(lslib_handle *h);
static int lslib_reset_device(lslib_handle *);
static int lslib_get_device_descriptor(lslib_handle *h, struct libusb_device_descriptor *desc);
static int lslib_get_busnumber(lslib_handle *h);
static int lslib_get_devaddr(lslib_handle *h);
static int lslib_kernel_driver_active(lslib_handle *h, int interface);
static int lslib_claim_interface(libusb_device_handle *h, int interface);
static int lslib_control_write (lslib_handle *h, unsigned char bmRequestType, unsigned char bRequest,
        unsigned short wValue, unsigned short wIndex, unsigned char *data, unsigned short wLength,
        unsigned int timeout);
static int lslib_control_read (lslib_handle *h, unsigned char bmRequestType, unsigned char bRequest,
        unsigned short wValue, unsigned short wIndex, unsigned char *data, unsigned short wLength,
        unsigned int timeout);
static int a1InitProcedure(lsdev *h);

char *lslib_get_dev_name(device_type dd)
{    
    std::vector <char *> device_names = {"NONE", "LS7", "LS2FP", "LS1FP", "LSPP", "LSPPFP", "LSPFP", "LS1FP10", "LS2FP10", "LS1FP_M", "LS8FP", "LS_A1", "LS_A2", "LS_A4"};
    static char temp[128];
    if (dd < device_names.size())
        return device_names.at(dd);
    else
        sprintf(temp, "Not found type %d", (int)dd);
    return temp;
}

int lslib_control_contrast (lsdev * dev, operation rw)
{
    int res = 0;
    unsigned char data[2] = {0,0};

    if (rw) {
        data[0] = dev->contrast;
    }
    if ((res = sensor_control (dev->h, SENS_REG_GAIN, data, rw, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;

    if (!rw)
        dev->contrast = data[1];

    return LSLIB_NO_ERROR;
}

int lslib_control_brightness (lsdev * dev, operation rw)
{
    int res = 0;
    short data[4] = {0,0,0,0};

    if (dev->type == LS8FP)
    {

        if (dev->bright.ee < 0) data[0] |= (0x1<<8)|(256+(uchar)dev->bright.ee);
        else data[0] = (uchar)dev->bright.ee;

        if (dev->bright.oe < 0) data[1] |= (0x1<<8)|(256+(uchar)dev->bright.oe);
        else data[1] = (uchar)dev->bright.oe;

        if (dev->bright.eo < 0) data[2] |= (0x1<<8)|(256+(uchar)dev->bright.eo);
        else data[2] = (uchar)dev->bright.eo;

        if (dev->bright.oo < 0) data[3] |= (0x1<<8)|(256+(uchar)dev->bright.oo);
        else data[3] = (uchar)dev->bright.oo;

        res = sensor_control (dev->h, SENS_REG_OFFSET1, (unsigned char *)&data[0], rw, MT9_SENS_ADDR);
        res = sensor_control (dev->h, SENS_REG_OFFSET2, (unsigned char *)&data[1], rw, MT9_SENS_ADDR);
        res = sensor_control (dev->h, SENS_REG_OFFSET3, (unsigned char *)&data[2], rw, MT9_SENS_ADDR);
        res = sensor_control (dev->h, SENS_REG_OFFSET4, (unsigned char *)&data[3], rw, MT9_SENS_ADDR);

        if (rw == LSLIB_READ)
        {
            if (data[0]&0x1) dev->bright.ee = ((data[0]>>8)&0xFF-256);
            else dev->bright.ee = data[0] >> 8;
            if (data[1]&0x1) dev->bright.oe = ((data[1]>>8)&0xFF-256);
            else dev->bright.oe = data[1] >> 8;
            if (data[2]&0x1) dev->bright.eo = ((data[2]>>8)&0xFF-256);
            else dev->bright.eo = data[2] >> 8;
            if (data[3]&0x1) dev->bright.oo = ((data[3]>>8)&0xFF-256);
            else dev->bright.oo = data[3] >> 8;
        }
    }

    if (dev->type == LS_A1)
    {
        uint16_t preExpo = dev->bright.average();
        if (preExpo > 63) preExpo = 63;
        uint16_t expo = preExpo*0xF;
        libls_mt9m001(dev, 0x3012, (quint8 *)&expo, 1);
    }

    if (dev->type == LS_A2)
    {
        uint16_t expo = dev->bright.average()*0x7;
        libls_mt9m001(dev, 0x9, (quint8 *)&expo, 1);
    }
    return res;
}

int lslib_control_backlight (lsdev * dev, operation rw)
{
    int res = 0;
    unsigned char bl = 255 - dev->bckl.bot_left_bl;
    unsigned char br = 255 - dev->bckl.bot_right_bl;
    unsigned char tl = 255 - dev->bckl.top_left_bl;
    unsigned char tr = 255 - dev->bckl.top_right_bl;

    if (rw == LSLIB_WRITE)
    {
        if ((res =  vendor_write(dev->h, 0, 1, &bl, LS2_BOTTOM_LEFT_LIGHT)) != 1) return LSLIB_I2C_ERROR;
        if ((res =  vendor_write(dev->h, 0, 1, &br, LS2_BOTTOM_RIGHT_LIGHT)) != 1) return LSLIB_I2C_ERROR;
        if ((res =  vendor_write(dev->h, 0, 1, &tl, LS2_TOP_LEFT_LIGHT)) != 1) return LSLIB_I2C_ERROR;
        if ((res =  vendor_write(dev->h, 0, 1, &tr, LS2_TOP_RIGHT_LIGHT)) != 1) return LSLIB_I2C_ERROR;
    } else {
        if ((res =  vendor_read(dev->h, 0, 1, &bl, LS2_BOTTOM_LEFT_LIGHT)) != 1) return LSLIB_I2C_ERROR;
        if ((res =  vendor_read(dev->h, 0, 1, &br, LS2_BOTTOM_RIGHT_LIGHT)) != 1) return LSLIB_I2C_ERROR;
        if ((res =  vendor_read(dev->h, 0, 1, &tl, LS2_TOP_LEFT_LIGHT)) != 1) return LSLIB_I2C_ERROR;
        if ((res =  vendor_read(dev->h, 0, 1, &tr, LS2_TOP_RIGHT_LIGHT)) != 1) return LSLIB_I2C_ERROR;
        dev->bckl.bot_left_bl = 255 - bl;
        dev->bckl.bot_right_bl = 255 - br;
        dev->bckl.top_left_bl = 255 - tl;
        dev->bckl.top_right_bl = 255 - tr;
    }
    return res;
}



static int device_is_of_interest(libusb_device *d)
{
    struct libusb_device_descriptor desc;

    if(libusb_get_device_descriptor(d, &desc)) return 0;

    int list_len = sizeof(vpd)/sizeof(struct VPD);

    for (int i = 0; i < list_len; i++)
    {
        if ( (vpd[i].vid == desc.idVendor) && (vpd[i].pid == desc.idProduct) ) return 1;
    }

    return 0;
}

static unsigned short lslib_getvendor(lslib_handle *h)
{
    struct libusb_device_descriptor d;
    lslib_get_device_descriptor(h, &d);
    return d.idVendor;
}

static unsigned short lslib_getproduct(lslib_handle *h)
{
    struct libusb_device_descriptor d;
    lslib_get_device_descriptor(h, &d);
    return d.idProduct;
}

int lslib_control_framesize (lsdev *dev, operation rw)
{
    unsigned char buf[8];
    int res = 0;
    lsframe * frame = &dev->frame;

    if (dev->type < LS_A1)
    {
        if (rw == LSLIB_READ) {
            if ((res = sensor_control (dev->h, SENS_REG_WTOP, &buf[0], rw, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, SENS_REG_WLEFT, &buf[2], rw, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, SENS_REG_WHEIGHT, &buf[4], rw, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, SENS_REG_WWIDTH, &buf[6], rw, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;

            frame->top = buf[1] | buf[0] << 8;
            frame->left = buf[3] | buf[2] << 8;
            frame->height = 1 + (buf[5] | buf[4] << 8);
            frame->width = 1 + (buf[7] | buf[6] << 8);


    //        frame->height = 1024;
    //        frame->width = 1280;

            dev->frame.size = frame->width * frame->height;
        } else
        {
            frame->top &= 0x7FF;
            frame->left &= 0x7FE; // must be even number

            frame->width &= 0x7FE; // must be even number
            frame->height &= 0x7FE; //

            dev->frame.size = frame->width * frame->height;

            frame->width --;
            frame->height --;
            if ((res = sensor_control (dev->h, SENS_REG_WTOP, (unsigned char *)&frame->top, rw, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, SENS_REG_WLEFT, (unsigned char *)&frame->left, rw, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, SENS_REG_WHEIGHT, (unsigned char *)&frame->height, rw, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, SENS_REG_WWIDTH, (unsigned char *)&frame->width, rw, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
        }
    }

    if (dev->type == LS_A1)
    {
        uint16_t start_y = 0, start_x = 0, end_y = 0, end_x = 0;
        if (rw == LSLIB_READ) {
            if ((res = sensor_control (dev->h, 0x3002, (uint8_t *)&start_y, 0, AR0131_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 0x3004, (uint8_t *)&start_x, 0, AR0131_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 0x3006, (uint8_t *)&end_y, 0, AR0131_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 0x3008, (uint8_t *)&end_x, 0, AR0131_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            frame->top = start_y;
            frame->left = start_x;
            frame->height = end_y - start_y + 1;
            frame->width = end_x - start_x;
        }
        else
        {
            start_y = frame->top;
            start_x = frame->left;
            end_y = frame->height + start_y - 1;
            end_x = frame->width + start_x;
            if ((res = sensor_control (dev->h, 0x3002, (unsigned char *)&start_y, 1, AR0131_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 0x3004, (unsigned char *)&start_x, 1, AR0131_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 0x3006, (unsigned char *)&end_y, 1, AR0131_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 0x3008, (unsigned char *)&end_x, 1, AR0131_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
        }
        dev->frame.size = frame->width * frame->height;
        dev->frame.embedded_data_begin_size = dev->frame.width*2 + 2;
        dev->frame.embedded_data_end_size = dev->frame.width*2 + 2;
    }

    if (dev->type == LS_A2)
    {
        uint16_t start_y = 0, start_x = 0, end_y = 0, end_x = 0;
        if (rw == LSLIB_READ) {
            if ((res = sensor_control (dev->h, 1, (uint8_t *)&start_y, 0, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 2, (uint8_t *)&start_x, 0, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 3, (uint8_t *)&end_y, 0, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 4, (uint8_t *)&end_x, 0, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            frame->top = start_y;
            frame->left = start_x;
            frame->height = end_y + 1;
            frame->width = end_x + 1;
        }
        else
        {
            start_y = frame->top;
            start_x = frame->left;
            end_y = frame->height - 1;
            end_x = frame->width - 1;
            if ((res = sensor_control (dev->h, 1, (unsigned char *)&start_y, 1, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 2, (unsigned char *)&start_x, 1, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 3, (unsigned char *)&end_y, 1, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 4, (unsigned char *)&end_x, 1, MT9_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;

            dev->frame.embedded_data_begin_size = 0;
            dev->frame.embedded_data_end_size = 0;
        }
        dev->frame.size = frame->width * frame->height;
    }

    if (dev->type == LS_A4)
    {
        uint16_t start_y = 0, start_x = 0, end_y = 0, end_x = 0;
        dev->frame.embedded_data_begin_size = 3332;
        if (rw == LSLIB_READ) {
            if ((res = sensor_control (dev->h, 0x3002, (uint8_t *)&start_y, 0, J003_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;   // starty
            if ((res = sensor_control (dev->h, 0x3004, (uint8_t *)&start_x, 0, J003_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;   // start_x
            if ((res = sensor_control (dev->h, 0x3006, (uint8_t *)&end_y, 0, J003_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR; // endy
            if ((res = sensor_control (dev->h, 0x3008, (uint8_t *)&end_x, 0, J003_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR; // endx
            frame->top = start_y;
            frame->left = start_x;
            frame->height = 1 + end_y - frame->top;
            frame->width = 1 + end_x - frame->left;
        }
        else
        {
            start_y = frame->top;
            start_x = frame->left;
            end_y = frame->height + start_y - 1;
            end_x = frame->width + start_x - 1;
            if ((res = sensor_control (dev->h, 0x3002, (unsigned char *)&start_y, 1, J003_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 0x3004, (unsigned char *)&start_x, 1, J003_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 0x3006, (unsigned char *)&end_y, 1, J003_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;
            if ((res = sensor_control (dev->h, 0x3008, (unsigned char *)&end_x, 1, J003_SENS_ADDR)) != 2) return LSLIB_SENS_ERROR;

            dev->frame.embedded_data_end_size = 0;
        }

        dev->frame.size = frame->width * frame->height;
    }

    return 0;
}

int lslib_GetType(lsdev *dev, pdevice_type pValue, unsigned char *pixel_width)
{
    int res = 0;
    unsigned char data[4];
    if (0 > (res = lslib_control_read(
                dev->h,
                LIBUSB_REQUEST_TYPE_VENDOR
                | LIBUSB_RECIPIENT_DEVICE,
                0xD4,
                0x0,
                0,
                data,
                2,
                100))) {

        perror("Error request type:");
        return res;
    }
    *pValue = (device_type)data[0];
    *pixel_width = data[1];

    return res;
}

static unsigned int GetSerial(lslib_handle *h)
{
    unsigned int serial;
    if (vendor_read(h, 0x1DFE, 4, (unsigned char *)&serial, 0x5101))
        return serial;
    else return -1;
}

int lslib_GetDeviceTree( lsdev *dt )
{
    libusb_device_handle *h = NULL;
    libusb_device **list;
    device_type dev_type = NOFLEX;

    int numdev = libusb_get_device_list(NULL, &list);

    if ( numdev < 0 ) {
       libusb_free_device_list(list, 1);
       return -1;
    }

    nid = 0;

    for (int i = 0; i < numdev; ++i )
    {
        libusb_device *tdev = list[i];

        if ( device_is_of_interest(tdev) )
        {
            int r = libusb_open(tdev, &h);

            if (r) {
                continue;
            }
            dt[nid].dev_num     = i;
            dt[nid].h           = (lslib_handle *)h;
            dt[nid].vid       = lslib_getvendor((lslib_handle *)h);
            dt[nid].pid       = lslib_getproduct((lslib_handle *)h);
            dt[nid].is_open   = 1;
            dt[nid].busnum    = lslib_get_busnumber((lslib_handle *)h);
            dt[nid].devaddr   = lslib_get_devaddr((lslib_handle *)h);

            if (dt[nid].pid == 0x8001)
                if (lslib_GetType(&dt[nid], &dev_type, &dt[nid].version) > 0)
                    strncpy(dt[nid].device_name, lslib_get_dev_name(dev_type), 8);

            ++nid;
            libusb_close(h);
        }
    }
    libusb_free_device_list(list, 1);

    return nid;
}

int lslib_init()
{
   const struct libusb_version *lv = libusb_get_version();
   qDebug() << "LIBUSB V" << lv->major << "." << lv->minor << "." << lv->micro << "." << lv->nano;
   return libusb_init(NULL);
}


void lslib_device_close(lsdev *h)
{
    if (h->h)
        libusb_close((libusb_device_handle *)h->h);
    h->h= NULL;
    return;
}

int lslib_device_open(int dev_num, lsdev *h)
{
    int r = 0;
    libusb_device **list;
    libusb_device_handle *hdev = NULL;    
    struct libusb_device_descriptor desc;

    int numdev = libusb_get_device_list(NULL, &list);
    if ( numdev < 0 ) {
       return LSLIB_NO_DEVICE;
    }

     if ( !device_is_of_interest(list[dev_num]) )
     {
         return LSLIB_NO_DEVICE;
     }


    r = libusb_open(list[dev_num], &hdev);
    r += libusb_get_device_descriptor(list[dev_num], &desc);
    r += lslib_claim_interface(hdev, 0);

    libusb_free_device_list(list, 1);

    if ( r ) {
        hdev = NULL;
        return r;
    }



    h->h = (lslib_handle *)hdev;
    h->pid = desc.idProduct;
    h->vid = desc.idVendor;
    h->type = NOFLEX;
    h->frame.embedded_data_begin_size = 0;
    h->frame.embedded_data_end_size = 0;

    if (h->pid != 0x8613)
    {
        if (lslib_GetType(h, &h->type, &h->version) == 2)
        {
            strncpy(h->device_name, lslib_get_dev_name(h->type), 8);
        } else
            qDebug()<< "Can't read device type!";




        if (h->type == LS_A1) //ar0131
        {
            qDebug()<< "LS_A1d device type!";
            a1InitProcedure(h);
        }




        if (h->type == LS_A2) // mt9p031
        {
                // prepare sensor
            h->frame.width = 2590;
            h->frame.height = 1940;
            h->frame.size = h->frame.width * h->frame.height;
            h->serial = GetSerial(h->h);
            uint16_t setup1 = 0x4306;
            sensor_control (h->h, 0x1e, (uchar *)&setup1, 1, MT9_SENS_ADDR);
//            lslib_control_framesize(h, LSLIB_READ);
//            lslib_control_contrast(h, LSLIB_READ);
//            lslib_control_brightness(h, LSLIB_READ);
//            lslib_control_backlight(h, LSLIB_READ);
//            sensor_enable_offset((lslib_handle *)hdev);
        }

        if (h->type == LS_A4) // mt9p031
        {
                // prepare sensor
            h->frame.width = 3664;
            h->frame.height = 2748;
            h->frame.size = h->frame.width * h->frame.height;
            h->frame.embedded_data_begin_size = 0;
            h->serial = GetSerial(h->h);

            uint16_t setup1 = 1;
            sensor_control (h->h, 0x301a, (uchar *)&setup1, 1, J003_SENS_ADDR);
            sleep(1);
//            setup1 = 0x8;
//            sensor_control (h->h, 0x304, (uchar *)&setup1, 1, J003_SENS_ADDR);
//            setup1 = 0x70;
//            sensor_control (h->h, 0x344, (uchar *)&setup1, 1, J003_SENS_ADDR);
//            setup1 = 0x1a08;
//            sensor_control (h->h, 0x300c, (uchar *)&setup1, 1, J003_SENS_ADDR);
//            setup1 = 0x40ff;
//            sensor_control (h->h, 0x3026, (uchar *)&setup1, 1, J003_SENS_ADDR);

             //uint16_t
            setup1 = 0x40ff;
            sensor_control (h->h, 0x3026, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0;
            sensor_control (h->h, 0x100, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x3;
            sensor_control (h->h, 0x300, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x1;
            sensor_control (h->h, 0x302, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x4;
            sensor_control (h->h, 0x304, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x28;
            sensor_control (h->h, 0x306, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0xa;
            sensor_control (h->h, 0x308, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x1;
            sensor_control (h->h, 0x30a, (uchar *)&setup1, 1, J003_SENS_ADDR);
            sleep(1);
            setup1 = 0x805;
            sensor_control (h->h, 0x3064, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x1;
            sensor_control (h->h, 0x104, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x111;
            sensor_control (h->h, 0x3016, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x70;
            sensor_control (h->h, 0x344, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0xebf;
            sensor_control (h->h, 0x348, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x8;
            sensor_control (h->h, 0x346, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0xac3;
            sensor_control (h->h, 0x34a, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x41;
            sensor_control (h->h, 0x3040, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x0;
            sensor_control (h->h, 0x400, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x10;
            sensor_control (h->h, 0x404, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0xe50;
            sensor_control (h->h, 0x34c, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0xabc;
            sensor_control (h->h, 0x34e, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x3025;
            sensor_control (h->h, 0x342, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0xb4b;
            sensor_control (h->h, 0x340, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x0;
            sensor_control (h->h, 0x202, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x3f2;
            sensor_control (h->h, 0x3014, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x9c;
            sensor_control (h->h, 0x3010, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x0;
            sensor_control (h->h, 0x3018, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x1080;
            sensor_control (h->h, 0x30d4, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x90b0;
            sensor_control (h->h, 0x306e, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x10;
            sensor_control (h->h, 0x3e00, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x0;
            sensor_control (h->h, 0x3178, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x1b24;
            sensor_control (h->h, 0x3ed0, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0xc3e4;
            sensor_control (h->h, 0x3edc, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x0;
            sensor_control (h->h, 0x3ee8, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x0;
            sensor_control (h->h, 0x104, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x1dc;
            sensor_control (h->h, 0x301a, (uchar *)&setup1, 1, J003_SENS_ADDR);
            setup1 = 0x301;
            sensor_control (h->h, 0x31ae, (uchar *)&setup1, 1, J003_SENS_ADDR);

//            setup1 = 0x2;
//            sensor_control (h->h, 0x315e, (uchar *)&setup1, 1, J003_SENS_ADDR);
//            lslib_control_framesize(h, LSLIB_READ);
//            lslib_control_contrast(h, LSLIB_READ);
//            lslib_control_brightness(h, LSLIB_READ);
//            lslib_control_backlight(h, LSLIB_READ);
//            sensor_enable_offset((lslib_handle *)hdev);
        }


    }
    return r;
}

char *lslib_error(int err)
{
    static char str[256];
    if ( err == -1 )
       sprintf(str, "Input/output error\n");
    else if ( err == -2 )
        sprintf(str, "Invalid parameter\n");
    else if ( err == -3 )
        sprintf(str, "Access denied (insufficient permissions)\n");
    else if ( err == -4 )
        sprintf(str, "No such device. Disconnected...?\n");
    else if ( err == -5 )
        sprintf(str, "Entity not found\n");
    else if ( err == -6 )
        sprintf(str, "Resource busy\n");
    else if ( err == -7 )
        sprintf(str, "Operation timed out\n");
    else if ( err == -8 )
        sprintf(str, "Overflow\n");
    else if ( err == -9 )
        sprintf(str, "Pipe error\n");
    else if ( err == -10 )
        sprintf(str, "System call interrupted, ( due to signal ? )\n");
    else if ( err == -11 )
        sprintf(str, "Insufficient memory\n");
    else if ( err == 12 )
        sprintf(str, "Operation not supported/implemented\n");
    else if ( err == 13 )
        sprintf(str, "Wrong device\n");
    else sprintf(str, "Unknown internal error\n");
    return str;
}

int vendor_read(lslib_handle *h, unsigned short addr, unsigned short len, unsigned char *data,  unsigned short i2caddr)
{
    int res = 0;
    if (0 > (res = lslib_control_read(
                h,
                LIBUSB_REQUEST_TYPE_VENDOR
                | LIBUSB_RECIPIENT_DEVICE,
                0xA2,
                addr,
                i2caddr,
                data,
                len,
                100)))
    {
         printf("Error vendor_read %s\r\n", libusb_strerror((libusb_error)res));
    }
    if (i2caddr != EERPOM_ADDR) {
        for (int i = 0; i < len; i+=2)
        {
            unsigned char rd = data[i];
            data[i] = data[i+1];
            data[i+1] = rd;
        }
    }
    return res;
}

int vendor_write(lslib_handle *h, unsigned short addr, unsigned short len, unsigned char *data,  unsigned short i2caddr)
{
    int res = 0;
    if (0 > (res = lslib_control_write(
                h,
                LIBUSB_REQUEST_TYPE_VENDOR
                | LIBUSB_RECIPIENT_DEVICE,
                0xA2,
                addr,
                i2caddr,
                data,
                len,
                200)))
    {
         printf("Error vendor_write %s\r\n", libusb_strerror((libusb_error)res));
    }

    return res;
}

void lslib_close(void)
{
    libusb_exit(NULL);
}

static int lslib_get_busnumber(lslib_handle *h)
{
    libusb_device *tdev = libusb_get_device((libusb_device_handle *)h);
    return libusb_get_bus_number( tdev );
}

static int lslib_get_devaddr(lslib_handle *h)
{
    libusb_device *tdev = libusb_get_device((libusb_device_handle *)h);
    return libusb_get_device_address( tdev );
}


//int vendor_read(lslib_handle *h, unsigned short addr, unsigned short len, unsigned char *data,  unsigned short i2caddr)
int lslib_GetROM(lsdev *hDev, unsigned char * pData, unsigned  int len)
{
    if (len > 512) return 0;
    return vendor_read(hDev->h, 0x1E00, len, pData, EERPOM_ADDR);
}

int lslib_SetROM(lsdev *hDev, unsigned char * pData, unsigned  int len)
{
    if (len > 512) return 0;
    return vendor_write(hDev->h, 0x1E00, len, pData, EERPOM_ADDR);
}

int lslib_WriteFirmware(lsdev *hDev, unsigned char * pData, unsigned  int len, void (*f)(int))
{
#define BLOCK 128
    unsigned long fsize = len;
    unsigned long nBytes = 0;
    bool bRet = true;
    unsigned short Addr = 0;
    unsigned char *p = pData;
    int percentOfLoad = 0;

    if (len > 0x1E00) {
        return LSLIB_ROM_SIZE_OVF;
    }

    while (fsize) {


        if (fsize > BLOCK) nBytes = BLOCK;
        else nBytes = fsize;
        fsize -= nBytes;
        bRet = vendor_write(hDev->h, Addr, nBytes, p, EERPOM_ADDR);

        if (!bRet) {
            printf("Flexing error\r\n");
            return LSLIB_ROM_WRITE_ERR;
        }
        percentOfLoad = 100 * (1 - (double)fsize/(double)len);
        Addr += nBytes;
        p+=nBytes;
        if (f != nullptr) {
            f(percentOfLoad);
        }
    }

    return LSLIB_NO_ERROR;
}


static int lslib_claim_interface(libusb_device_handle *h, int interface)
{
    return ( libusb_claim_interface(h, interface) );
}

//static int lslib_kernel_driver_active(lslib_handle *h, int interface)
//{
//    return ( libusb_kernel_driver_active(h, interface) );
//}

static int lslib_get_device_descriptor(lslib_handle *h, struct libusb_device_descriptor *desc)
{
    libusb_device *tdev = libusb_get_device((libusb_device_handle *)h);
    return ( libusb_get_device_descriptor(tdev, desc ) );
}


static int lslib_control_transfer(lslib_handle *h, unsigned char bmRequestType, unsigned char bRequest,
        unsigned short wValue, unsigned short wIndex, unsigned char *data, unsigned short wLength,
        unsigned int timeout)
{
    return ( libusb_control_transfer((libusb_device_handle *)h, bmRequestType, bRequest, wValue, wIndex, data, wLength, timeout) );
}

static int lslib_control_read (lslib_handle *h, unsigned char bmRequestType, unsigned char bRequest,
        unsigned short wValue, unsigned short wIndex, unsigned char *data, unsigned short wLength,
        unsigned int timeout)
{

    int ret = libusb_control_transfer((libusb_device_handle *)h, bmRequestType | 0x80, bRequest, wValue, wIndex, data, wLength, timeout) ;

    return ret;
}

static int lslib_control_write (lslib_handle *h, unsigned char bmRequestType, unsigned char bRequest,
        unsigned short wValue, unsigned short wIndex, unsigned char *data, unsigned short wLength,
        unsigned int timeout)
{

    int ret = libusb_control_transfer((libusb_device_handle *)h, bmRequestType & 0x7F, bRequest, wValue, wIndex, data, wLength, timeout) ;

    return ( ret );
}

static inline int lslib_bulk_transfer(lslib_handle *h, unsigned char endpoint, unsigned char *data, int length,
                int *transferred, int timeout)
{

    int ret = libusb_bulk_transfer((libusb_device_handle *)h, endpoint, data, length, transferred, timeout);

    return ( ret );
}



//---------------------------------------------------------------------------
// Загрузить блок во внутреннюю память FX2
//int AnchorDownload(HANDLE hDev, WORD Addr, WORD Len, PBYTE Data)
//{
//  DWORD dwRet;
//  WORD Offset = Addr;

//  memcpy(pMem, Data, Len);

//  return DeviceIoControl(hDev,
//                         IOCTL_USBCAM_ANCHOR_DOWNLOAD,
//                         &Offset,
//                         sizeof(WORD),
//                         pMem,
//                         Len,
//                         &dwRet,
//                         NULL);
//}
//---------------------------------------------------------------------------
// Загрузить программу "Загрузчик"
/*int DownLoader(HANDLE hDev)
{
  PINTEL_HEX_RECORD ptr = Vend_Ax_Fx2;

  if(!pMem) return FALSE;
  if(!HoldDev(hDev)) return FALSE;

  while (ptr->Type == 0) {
    AnchorDownload(hDev, ptr->Address, ptr->Length, ptr->Data);
    ptr++;
  }
  return RunDev(hDev);
}

// Для загрузки HEX файла в FX2
int DownloadFirmware (lslib_handle *h, char * pHexFileName)
{
    lslib_control_transfer
  BOOL bRet = FALSE;
  TMemCache *pMemCache;
  int i;

  do {
    pMemCache = (TMemCache *)LocalAlloc(LMEM_FIXED,sizeof(TMemCache));
    if (!pMemCache) {
//      MessageBox(NULL,"Insufficient resources","DownloadFirmware",MB_OK);
      break;
    }

    pMemCache->pImg = (TMemImg *)LocalAlloc(LMEM_FIXED,TGT_IMG_SIZE);
    if (!pMemCache->pImg) {
//      MessageBox(NULL,"Insufficient resources","DownloadFirmware",MB_OK);
      break;
    }

    bRet = FileToCache(pMemCache, pHexFileName);
    if (!bRet) break;

    // check for high mem first, load loader first if necessary
    for (i = 0; i < pMemCache->nSeg; i++) {
      if (pMemCache->pSeg[i].Addr >= 0x2000) {
        bRet = DownLoader(hDev);
        break;
      }
    }
    if(!bRet) {
//      MessageBox(NULL,"Error load Downloader","DownloadFirmware",MB_OK);
      break;
    }

    // Загрузка во внешнюю память
    for (i = 0; i < pMemCache->nSeg; i++) {
      if (pMemCache->pSeg[i].Addr >= 0x2000) {
        bRet = DownLoadHigh(hDev, pMemCache->pSeg[i].Addr,
                 pMemCache->pSeg[i].Size, pMemCache->pSeg[i].pData);
        if(!bRet){
//          MessageBox(NULL,"Error DownLoadHigh","DownloadFirmware",MB_OK);
          break;
        }
      }
    }
    if(!bRet) break;

    bRet = HoldDev(hDev);
    if(!bRet){
//      MessageBox(NULL,"Error HoldDev","DownloadFirmware",MB_OK);
      break;
    }

    // Загрузка во внутреннюю память
    for (i = 0; i < pMemCache->nSeg; i++) {
      if (pMemCache->pSeg[i].Addr < 0x2000) {
        bRet = AnchorDownload(hDev, pMemCache->pSeg[i].Addr,
                 pMemCache->pSeg[i].Size, pMemCache->pSeg[i].pData);
        if(!bRet){
//          MessageBox(NULL,"Error AnchorDownload","DownloadFirmware",MB_OK);
          break;
        }
      }
    }
    if(!bRet) break;

    bRet = RunDev(hDev);
    if(!bRet){
//      MessageBox(NULL,"Error RunDev","DownloadFirmware",MB_OK);
    }
  } while(0);

  if (pMemCache) {
    if (pMemCache->pImg) LocalFree(pMemCache->pImg);
    LocalFree(pMemCache);
  }
  return bRet;
}
*/


//static void control_transfer(lslib_handle *h, unsigned int address, unsigned char *dbuf, int len)
//{
//    int j;
//    int r;
//    int b;
//    unsigned int *pint;
//    int index;

//    int balance = len;
//    pint = (unsigned int *)dbuf;

//    index = 0;
//    while ( balance > 0 ) {
//          if ( balance > 4096 )
//         b = 4096;
//          else b = balance;
//          r = lslib_control_transfer(h, 0x40, 0xA0, ( address & 0x0000ffff ), address >> 16, &dbuf[index], b, 1000);
//          if ( r != b ) {
//           printf("Error in control_transfer\n");
//          }
//          address += b ;
//          balance -= b;
//          index += b;
//    }
//    for ( j = 0; j < len/4; ++j )
//        checksum += pint[j];
//}

//int light_control (lslib_handle *h, unsigned short bl_addr, unsigned char value)
//{
//    unsigned char data = 255-value;
//    return vendor_write(h, 0, 1, &data, bl_addr);
//}

static int sensor_enable_offset(lslib_handle *h)
{
    unsigned short value = 0x499;
    int res = vendor_write(h, 0x62, 2, (unsigned char *)&value, MT9_SENS_ADDR);
    return res;
}

int lslib_soi968 (lsdev *hDev, unsigned short reg, unsigned char *buf, int write)
{
    int res = 0;
    if (write) {
        res = vendor_write(hDev->h, reg, 2, buf, SOI968_SENS_ADDR);
    } else
        res = vendor_read(hDev->h, reg, 1, buf, SOI968_SENS_ADDR);

    return res;
}

int libls_sensor (lsdev *hDev, unsigned short sens_addr, unsigned short reg, unsigned char *buf, int write)
{
    return sensor_control (hDev->h, reg, buf, write, (sens_addr << 8) | 3);
}

int libls_mt9m001 (lsdev *hDev, unsigned short reg, unsigned char *buf, int write)
{
    if (hDev->type == LS_A1)
        return sensor_control (hDev->h, reg, buf, write, AR0131_SENS_ADDR);
    if (hDev->type == LS_A2)
        return sensor_control (hDev->h, reg, buf, write, MT9_SENS_ADDR);
    if (hDev->type == LS_A4)
        return sensor_control (hDev->h, reg, buf, write, J003_SENS_ADDR);


    return -1;
}

static int sensor_control (lslib_handle *h, unsigned short reg, unsigned char *buf, int write, unsigned short sensorAddr)
{
    int res = 0;
    if (write) {
        res = vendor_write(h, reg, 2, buf, sensorAddr);
    } else
        res = vendor_read(h, reg, 2, buf, sensorAddr);

    return res;
}

int lslib_flowctl(lsdev *hDev, int start)
{
#define req_res 4
    unsigned char answer[req_res];
    int res = 0;
    res = lslib_control_read(
                hDev->h,
                LIBUSB_REQUEST_TYPE_VENDOR
                | LIBUSB_RECIPIENT_DEVICE,
                0xD1,
                0,
                start,
                answer,
                req_res,
                100);

    if (res != req_res)
        perror("frame request error: ");

    return res;
}
extern quint16 tim;
static int req_frame(lslib_handle *h)
{
#define req_res 4
    unsigned char answer[req_res];
    int res = 0;
    res = lslib_control_write(
                h,
                LIBUSB_REQUEST_TYPE_VENDOR
                | LIBUSB_RECIPIENT_DEVICE,
                0xD0,
                0,
                0,
                answer,
                2,
                100);

//    if (res != req_res)
//        perror("frame request error: ");

    return res;
}

int lslib_a1FX2Reset(lsdev *dev)
{
    unsigned char answer[req_res];
    int res = 0;
    res = lslib_control_read(
                dev->h,
                LIBUSB_REQUEST_TYPE_VENDOR
                | LIBUSB_RECIPIENT_DEVICE,
                0xB8,
                0,
                0,
                answer,
                1,
                100);



    return res;
}
int lslib_a1SensorReset(lsdev *dev)
{
    unsigned char answer[req_res];
    int res = 0;
    res = lslib_control_read(
                dev->h,
                LIBUSB_REQUEST_TYPE_VENDOR
                | LIBUSB_RECIPIENT_DEVICE,
                0xD6,
                0,
                0,
                answer,
                2,
                100);

    unsigned short d1 = 0x10dc;
    unsigned short d2 = 0x10;
    unsigned short d3 = 0x11dc;
    unsigned short d4 = 0x1982;
    unsigned short d5 = 0x11de;

//    BYTE xdata REG02[4]={0x30, 0x2a,0x00,0x10};
//    BYTE xdata REG03[4]={0x30, 0x1a,0x11,0xdc};
//    BYTE xdata REG04[4]={0x30, 0x64,0x19,0x82};
//    BYTE xdata REG05[4]={0x30, 0x1a,0x11,0xde};

    if (sensor_control (dev->h, 0x301A, (unsigned char *)&d1, 1, AR0131_SENS_ADDR) != 2) return LSLIB_SENS_ERROR;
    if (sensor_control (dev->h, 0x302A, (unsigned char *)&d2, 1, AR0131_SENS_ADDR) != 2) return LSLIB_SENS_ERROR;
    if (sensor_control (dev->h, 0x301A, (unsigned char *)&d3, 1, AR0131_SENS_ADDR) != 2) return LSLIB_SENS_ERROR;
    if (sensor_control (dev->h, 0x3064, (unsigned char *)&d4, 1, AR0131_SENS_ADDR) != 2) return LSLIB_SENS_ERROR;
    if (sensor_control (dev->h, 0x301A, (unsigned char *)&d5, 1, AR0131_SENS_ADDR) != 2) return LSLIB_SENS_ERROR;

    return a1InitProcedure(dev);
}

static int a1InitProcedure(lsdev *h) {

    h->frame.top = 2;
    h->frame.left = 0;
    h->frame.height = 960;
    h->frame.width = 1280;

    lslib_control_framesize(h, LSLIB_WRITE);
    // setup sensor speed fPIXCLK= (fEXTCLK × M) / (N × P1 x P2 ) fEXTCLK = 24MHz,
    // EXTCLK -> devide pllN -> multiply M (VCO) -> devide P1 (system freq) -> devide P2 (pix_clk)
    // 384 < VCO < 768 ( not real ), actually VCO may be lower 100.
    uint16_t pllM = 0x10; // 32 < P2 < 384
    if (sensor_control (h->h, 0x3030, (uchar *)&pllM, 1, AR0131_SENS_ADDR) != 2) return LSLIB_I2C_ERROR;
    uint16_t pllN = 0x2;    // 1 < N < 64
    if (sensor_control (h->h, 0x302e, (uchar *)&pllN, 1, AR0131_SENS_ADDR)  != 2) return LSLIB_I2C_ERROR;
    uint16_t pllP1 = 0x1;   // 1 < P1 < 16
    if (sensor_control (h->h, 0x302c, (uchar *)&pllP1, 1, AR0131_SENS_ADDR)  != 2) return LSLIB_I2C_ERROR;
    uint16_t pllP2 = 0x10;  // 1 < P1 < 16
    if (sensor_control (h->h, 0x302a, (uchar *)&pllP2, 1, AR0131_SENS_ADDR)  != 2) return LSLIB_I2C_ERROR;
    return LSLIB_NO_ERROR;
}

int lslib_vendor_req(lsdev *dev, quint8 req, quint16 value, quint16 index, quint8 *data, quint16 len)
{
    return lslib_control_read(
                dev->h,
                LIBUSB_REQUEST_TYPE_VENDOR
                | LIBUSB_RECIPIENT_DEVICE,
                req,
                value,
                index,
                data,
                len,
                100);
}


int lslib_flow(lsdev *dev, unsigned char *buff)
{
    int len = 0;
    unsigned int transfer_size = dev->frame.size;
    int over_l = 0;


    lslib_bulk_transfer(dev->h, 0x82, buff, transfer_size, &len, 500);

    if (len != transfer_size) {
        qDebug() << QTime().currentTime().toString("hh.mm.ss.z") << " invalid len" << len << " Must be " << transfer_size;
        return FRAME_ERROR;
    }


    return 0;
}

void save_debug_frame(quint8 *buff, quint32 len)
{
    static quint32 frame_counter = 0;
    char filename[100];
    sprintf(filename, "data%d.bin", frame_counter++);
    FILE *pf = fopen(filename, "wb");
    if (pf)
    {
     fwrite(buff, 1, len, pf);
     fclose(pf);
    }
}
//quint32 transfer_count = 0;
//quint32 transfer_done = 0;
//void LIBUSB_CALL callback(struct libusb_transfer *xfr)
//{
//    if (xfr->status!=LIBUSB_TRANSFER_COMPLETED) {
//        qDebug() << "Transfer failed: " << xfr->status;
//    }
//    if (transfer_count < 308)
//        libusb_submit_transfer(xfr);
//    else
//    {
//        qDebug() << "Transfer done";
//        transfer_done = 1;
//    }
//}

int  lslib_getImage(lsdev *dev, unsigned char *buff)
{


//    quint8 *dat = NULL;
//    dat = libusb_dev_mem_alloc((libusb_device_handle *)dev->h, DAT_SZ);
//    if (dat == NULL)
//    {
//        qDebug() << "Cannot alloc mem";
//        return 0;
//    }

    if (req_frame(dev->h) < 0)  {
        return 0;
    }

    if (dev->type == LS_A1)
    {
        int len = 0;
        int transfer_size = dev->frame.size + dev->frame.embedded_data_begin_size + dev->frame.embedded_data_end_size;  // frame + stats
        lslib_bulk_transfer(dev->h, 0x82, buff, transfer_size, &len, 500);

        if (len != transfer_size) {
             qDebug() << QTime().currentTime().toString("hh.mm.ss.z") << " invalid len" << len << " Must be " << transfer_size;
             return FRAME_ERROR;
        } else {


            // check 1
            quint32 marker1 = ((quint32 *)buff)[0];
            if (marker1 != 0xA530AA0A)
            {
//                save_debug_frame(buff, transfer_size);
                qDebug() << "frame start marker error " << std::hex << marker1;
                return FRAME_ERROR;
            }
            // check 2
            quint16 marker2 = ((quint16 *)(buff + dev->frame.size + dev->frame.embedded_data_begin_size))[0];
            if (marker2 != 0x7B0B)
            {
                qDebug() << "frame end marker error " << std::hex << marker2;
//                save_debug_frame(buff, transfer_size);
                return FRAME_ERROR;
            }

         }
    }

    if (dev->type == LS_A2)
    {
        #define DAT_SZ 1024*1024*1
        quint8 dat[DAT_SZ];
        int len = 0;
        libusb_device_handle *d = (libusb_device_handle *)dev->h;
        quint32 transfer_size = 49152-4; //16380;//20476;//dev->frame.size;  // frame + stats

        quint32 transfer_count = dev->frame.size/(transfer_size-12);


        for (int i = 0; i < transfer_count; i++) {
            int r = libusb_bulk_transfer(d, 0x82, dat, transfer_size, &len, 500);// lslib_bulk_transfer(dev->h, 0x82, dat, transfer_size-4, &len, 500);

            if (r) {
                qDebug() << "Error " << r;
                 libusb_clear_halt(d, 0x82);
            }

            if (len != transfer_size) {
                 qDebug() << QTime().currentTime().toString("hh.mm.ss.z") << " len" << len << "at" << i<< "Must be" << transfer_size-4;
                 qDebug() << "tr count " << transfer_count;
              //   libusb_dev_mem_free((libusb_device_handle *)dev->h, dat, DAT_SZ);
                return 0;//FRAME_ERROR;
            }
//            if (dat[2] != 0) {
//                qDebug() << "frame start marker error";
//            }
            memcpy(buff + i*(transfer_size-12), dat+12, (transfer_size-12));
        }
        quint32 tail_size = dev->frame.size - transfer_count*(transfer_size-12) + 16;

//        lslib_bulk_transfer(dev->h, 0x82, dat, 4716, &len, 500);
        libusb_bulk_transfer((libusb_device_handle *)dev->h, 0x82, dat, tail_size-4, &len, 500);

        if (len != (tail_size-4))
        {
            qDebug() << QTime().currentTime().toString("hh.mm.ss.z") << " len" << len << " Tail Must be " << tail_size-4;
            tail_size = tail_size - len;
//            libusb_bulk_transfer((libusb_device_handle *)dev->h, 0x82, dat + len, tail_size, &len, 500);
//            qDebug() << QTime().currentTime().toString("hh.mm.ss.z") << "tried to complete transfer. Len " << len;
            return FRAME_ERROR;
        }

         memcpy(buff + transfer_count*(transfer_size-12), dat+12, (tail_size-16));
    }

    if (dev->type == LS_A4)
    {
        #define DAT_SZ 1024*1024*1
        quint8 dat[DAT_SZ];
        int len = 0;
        libusb_device_handle *d = (libusb_device_handle *)dev->h;
        quint32 transfer_size = 49152;//20476;//dev->frame.size;  // frame + stats

        quint32 transfer_count = 102;// 5 + dev->frame.size/(transfer_size-12);
        // 8294400

        for (quint32 i = 0; i < transfer_count; i++) {
            int r = libusb_bulk_transfer(d, 0x82, dat, transfer_size-4, &len, 500);// lslib_bulk_transfer(dev->h, 0x82, dat, transfer_size-4, &len, 500);

            if (r) {
                qDebug() << "Error " << r;
                 libusb_clear_halt(d, 0x82);
            }

            if (len != transfer_size-4) {
                 qDebug() << QTime().currentTime().toString("hh.mm.ss.z") << " len" << len << "at" << i<< "Must be" << transfer_size-4;
              //   libusb_dev_mem_free((libusb_device_handle *)dev->h, dat, DAT_SZ);
                return 0;//FRAME_ERROR;
            }
//            if (dat[2] != 0) {
//                qDebug() << "frame start marker error";
//            }
            memcpy(buff + i*(transfer_size-16), dat+12, (transfer_size-16));
        }
        quint32 tail_size = 49152;
//        lslib_bulk_transfer(dev->h, 0x82, dat, 4716, &len, 500);
        libusb_bulk_transfer((libusb_device_handle *)dev->h, 0x82, dat, tail_size-4, &len, 500);

        if (len != (tail_size-4))
        {
            qDebug() << QTime().currentTime().toString("hh.mm.ss.z") << " len" << len << " Tail Must be " << tail_size-4;
            tail_size = tail_size - len;
//            libusb_bulk_transfer((libusb_device_handle *)dev->h, 0x82, dat + len, tail_size, &len, 500);
//            qDebug() << QTime().currentTime().toString("hh.mm.ss.z") << "tried to complete transfer. Len " << len;
            return 0;
        }

         memcpy(buff + transfer_count*(transfer_size-16), dat+12, (tail_size-16));
    }

 //   libusb_dev_mem_free((libusb_device_handle *)dev->h, dat, DAT_SZ);
    return 0;
}


////---------------------------------------------------------------------------
//// 4K Control EP0 transfer limit imposed by OS
//#define MAX_EP0_XFER_SIZE (1024*4)
////---------------------------------------------------------------------------
//// Обработка HEX файла и заполнение MemCache данными
//int FileToCache(TMemCache* pMemCache, char *pHexFileName)
//{
//  BOOL bRet = FALSE;
//  HANDLE hFile;
//  int i, recType, cnt;
//  int CNTFIELD, ADDRFIELD, RECFIELD, DATAFIELD;
//  PCHAR pstr, str;
//  PCHAR pBuf = NULL;
//  DWORD byte, addr, lenFile, totalRead;
//  PBYTE ptr;

//  FILE *pf;

//  pf = fopen(pHexFileName, "wb");

//  if(pf == NULL) {
////    MessageBox(NULL,"Error CreateFile","FileToCache",MB_OK);
//    return -1;
//  }

//  do {

//      lenFile = GetFileSize( hFile, NULL);

//    if(lenFile < 10 || lenFile > 0x20000) {
////      MessageBox(NULL,"Bad Length File","FileToCache",MB_OK);
//      break;
//    }

//    pBuf = (PCHAR)LocalAlloc(LMEM_FIXED, lenFile + 1);
//    if (!pBuf) {
////      MessageBox(NULL,"Insufficient resources","FileToCache",MB_OK);
//      break;
//    }

//    bRet = ReadFile(hFile, pBuf, lenFile, &totalRead, NULL);
//    if(!bRet) {
////      MessageBox(NULL,"Error Read File","FileToCache",MB_OK);
//      break;
//    }
//    pstr = pBuf;
//    pstr[lenFile] = '\0';

//    // offsets of fields within record -- may change later due to "spaces"
//    // setting
//    CNTFIELD = 1;
//    ADDRFIELD = 3;
//    RECFIELD = 7;
//    DATAFIELD = 9;

//    addr = 0;
//    pMemCache->nSeg = 0;
//  bRet = FALSE;

//    while (*pstr) {
//      str = pstr;

//      // Поиск следующей строки
//      while(*pstr++ != '\r');
//      *(pstr - 1) = '\0';
//      pstr++; // указывает на следующую строку или на '\0'

//      if (str[0] != ':') {
////        MessageBox(NULL,"Bad Format HexFile","FileToCache",MB_OK);
//        break;
//      }

//      if (str[1]==' ') { // get the record type
//        CNTFIELD = 1 + 1;
//        ADDRFIELD = 3 + 2;
//        RECFIELD = 7 + 3;
//        DATAFIELD = 9 + 4;
//      }

//      sscanf(str+RECFIELD, "%2x", &recType);

//      ptr = (PBYTE)pMemCache->pImg;
//      switch(recType) {
//        case 0: // data record
//          sscanf(str+CNTFIELD, "%2x", &cnt);
//          sscanf(str+ADDRFIELD, "%4x", &addr);
//          ptr += addr; // get pointer to location in image

//          if (pMemCache->nSeg &&
//              (pMemCache->pSeg[pMemCache->nSeg-1].Addr ==
//                 addr - pMemCache->pSeg[pMemCache->nSeg-1].Size) &&
//              (pMemCache->pSeg[pMemCache->nSeg-1].Size + cnt <= MAX_EP0_XFER_SIZE)) {
//            // если сегмент продолжает предыдущий, и их суммарная длина
//            // не будет превышать предела MAX_EP0_XFER_SIZE то их объединяем
//            pMemCache->pSeg[pMemCache->nSeg-1].Size += cnt;
//          }
//          else {
//            // start a new segment
//            pMemCache->pSeg[pMemCache->nSeg].Addr = (WORD)addr;
//            pMemCache->pSeg[pMemCache->nSeg].Size = (WORD)cnt;
//            pMemCache->pSeg[pMemCache->nSeg].pData = ptr;
//            pMemCache->nSeg++;
//          }

//          for (i = 0; i < cnt; i++) {
//            sscanf(str+DATAFIELD+i*2, "%2x", &byte);
//            *(ptr + i) = (CHAR)byte;
//          }
//        break;

//        case 1: //end record
//          bRet = TRUE;
//        break;
//      }
//    }
//  } while(0);

//  if(pf) fclose(pf);
//  if(pBuf) LocalFree(pBuf);

//  return 0;
//}

// Установить состояние бита CPUCS.0 в FX2
int ResetDevice(lsdev *h, unsigned char RestBit)
{
  unsigned char dwRet = RestBit;
//  VENDOR_REQUEST_IN VReq;

//  VReq.bRequest = 0xA0;
//  VReq.wValue = 0xE600; // using CPUCS.0 in FX2
//  VReq.wIndex = 0x00;
//  VReq.wLength = 0x01;
//  VReq.bData = RestBit;
//  VReq.direction = 0x00;

//  return DeviceIoControl (hDev,
//                          IOCTL_UsbCam_VENDOR_REQUEST,
//                          &VReq,
//                          sizeof(VENDOR_REQUEST_IN),
//                          NULL,
//                          0,
//                          &dwRet,
//                          NULL);

  lslib_control_write(
                  h->h,
                  LIBUSB_REQUEST_TYPE_VENDOR
                  | LIBUSB_RECIPIENT_DEVICE,
                  0xA0,
                  0xE600,
                  0,
                  &dwRet,
                  1,
                  100);


  return 0;
}

int HoldDev(lsdev * hDev)
{
  return ResetDevice(hDev, 1);
}

//---------------------------------------------------------------------------
// Запустить CPU FX2
int RunDev(lsdev * hDev)
{
  return ResetDevice(hDev, 0);
}

//int AnchorDownload(lsdev * h, WORD Addr, WORD Len, PBYTE Data)
//{
//  DWORD dwRet;
//  WORD Offset = Addr;

//  memcpy(pMem, Data, Len);

//  lslib_control_write(
//                  h->h,
//                  LIBUSB_REQUEST_TYPE_VENDOR
//                  | LIBUSB_RECIPIENT_DEVICE,
//                  0xA0,
//                  0xE600,
//                  0,
//                  &dwRet,
//                  1,
//                  100);

//  return DeviceIoControl(hDev,
//                         IOCTL_USBCAM_ANCHOR_DOWNLOAD,
//                         &Offset,
//                         sizeof(WORD),
//                         pMem,
//                         Len,
//                         &dwRet,
//                         NULL);
//    return 0;
//}

//int DownLoader(lsdev * hDev)
//{
//  PINTEL_HEX_RECORD ptr = Vend_Ax_Fx2;

//  while (ptr->Type == 0) {
//    AnchorDownload(hDev, ptr->Address, ptr->Length, ptr->Data);
//    ptr++;
//  }
//  return ResetDevice(hDev, 0);
//}

//int DownLoadHigh(lsdev * hDev, unsigned short Addr, unsigned short Len, unsigned char * pData)
//{
//  VENDOR_OR_CLASS_REQUEST_CONTROL Req;

//  Req.request = 0xA3;
//  Req.value = Addr;
//  Req.requestTypeReservedBits = 0;
//  Req.index = 0;
//  Req.direction = 0;   // 0=host to device, 1=device to host
//  Req.requestType = 2; // 1=class, 2=vendor
//  Req.recepient = 0;   // 0=device,1=interface,2=endpoint,3=other

//  memcpy(pMem, pData, Len);

//  return 0; // VendorRequest(hDev, &Req, Len);
//}

extern int lslib_downloadFirmware(lsdev *dev, unsigned char *pHexFileName);

//void DownloadFirmware(lsdev * hDev, PCHAR pHexFileName)
//{
//  BOOL bRet = FALSE;
//  TMemCache *pMemCache;
//  int i;

//  do {
//    pMemCache = (TMemCache *)LocalAlloc(LMEM_FIXED,sizeof(TMemCache));
//    if (!pMemCache) {
////      MessageBox(NULL,"Insufficient resources","DownloadFirmware",MB_OK);
//      break;
//    }

//    pMemCache->pImg = (TMemImg *)LocalAlloc(LMEM_FIXED,TGT_IMG_SIZE);
//    if (!pMemCache->pImg) {
////      MessageBox(NULL,"Insufficient resources","DownloadFirmware",MB_OK);
//      break;
//    }

//    bRet = FileToCache(pMemCache, pHexFileName);
//    if (!bRet) break;

//    // check for high mem first, load loader first if necessary
//    for (i = 0; i < pMemCache->nSeg; i++) {
//      if (pMemCache->pSeg[i].Addr >= 0x2000) {
//        bRet = DownLoader(hDev);
//        break;
//      }
//    }
//    if(!bRet) {
////      MessageBox(NULL,"Error load Downloader","DownloadFirmware",MB_OK);
//      break;
//    }

//    // Загрузка во внешнюю память
//    for (i = 0; i < pMemCache->nSeg; i++) {
//      if (pMemCache->pSeg[i].Addr >= 0x2000) {
//        bRet = DownLoadHigh(hDev, pMemCache->pSeg[i].Addr,
//                 pMemCache->pSeg[i].Size, pMemCache->pSeg[i].pData);
//        if(!bRet){
////          MessageBox(NULL,"Error DownLoadHigh","DownloadFirmware",MB_OK);
//          break;
//        }
//      }
//    }
//    if(!bRet) break;

//    bRet = HoldDev(hDev);
//    if(!bRet){
////      MessageBox(NULL,"Error HoldDev","DownloadFirmware",MB_OK);
//      break;
//    }

//    // Загрузка во внутреннюю память
//    for (i = 0; i < pMemCache->nSeg; i++) {
//      if (pMemCache->pSeg[i].Addr < 0x2000) {
//        bRet = AnchorDownload(hDev, pMemCache->pSeg[i].Addr,
//                 pMemCache->pSeg[i].Size, pMemCache->pSeg[i].pData);
//        if(!bRet){
////          MessageBox(NULL,"Error AnchorDownload","DownloadFirmware",MB_OK);
//          break;
//        }
//      }
//    }
//    if(!bRet) break;

//    bRet = RunDev(hDev);
//    if(!bRet){
////      MessageBox(NULL,"Error RunDev","DownloadFirmware",MB_OK);
//    }
//  } while(0);

//  if (pMemCache) {
//    if (pMemCache->pImg) LocalFree(pMemCache->pImg);
//    LocalFree(pMemCache);
//  }
//  return;
//}


int lslib_download_fx2(lsdev *h, char *filename)
{
    FILE *fp = NULL;
    char buf[256];
    char tbuf1[3];
    char tbuf2[5];
    char tbuf3[3];
    unsigned char reset = 0;
    int r;
    int count = 0;
    unsigned char num_bytes = 0;
    unsigned short address = 0;
    unsigned char *dbuf = NULL;
    int i;


    fp = fopen(filename, "r" );
    tbuf1[2] ='\0';
    tbuf2[4] = '\0';
    tbuf3[2] = '\0';

    reset = 1;
    r = lslib_control_transfer(h->h, 0x40, 0xA0, 0xE600, 0x00, &reset, 0x01, 1000);
    if ( !r ) {
       qDebug() << "Error in control_transfer\n";
       return r;
    }
    sleep(1);

    count = 0;

    while ( fgets(buf, 256, fp) != NULL ) {
        if ( buf[8] == '1' )
           break;
        strncpy(tbuf1,buf+1,2);
        num_bytes = strtoul(tbuf1,NULL,16);
        strncpy(tbuf2,buf+3,4);
        address = strtoul(tbuf2,NULL,16);
        dbuf = (unsigned char *)malloc(num_bytes);
        for ( i = 0; i < num_bytes; ++i ) {
            strncpy(tbuf3,&buf[9+i*2],2);
            dbuf[i] = strtoul(tbuf3,NULL,16);
        }
        r = lslib_control_transfer(h->h, 0x40, 0xA0, address, 0x00, dbuf, num_bytes, 1000);
        if ( !r ) {
           qDebug() << "Error in control_transfer\n";
           free(dbuf);
           return r;
        }
            count += num_bytes;
        free(dbuf);
    }
    qDebug() << "Total bytes downloaded " << count;
    sleep(1);
    reset = 0;
    r = lslib_control_transfer(h->h, 0x40, 0xA0, 0xE600, 0x00, &reset, 0x01, 1000);
    fclose(fp);
    return 0;
}
