#include "mw.h"
#include "ui_mw.h"
#include <QMessageBox>

#include <QDebug>
#include <qfiledialog.h>
#include <QStandardItemModel>
#include <QStandardItem>
#include <libusb.h>
#include <cam.h>
#include <QTime>
#include <qthread.h>
#include <QShortcut>
#include <include/lslib_global.h>
//#include <libcyapi/inc/CyAPI.h>
#define WW 2592
#define HH 1944
#define FRAME_SZ (WW*HH)
#define TRANS_NUM 20

static lsdev dev_tree[10];
quint8 dev_num = 0;
quint8 disp_mem[FRAME_SZ*2];
quint8 disp_mem_rgb[FRAME_SZ*4];

quint32 hist[255];

extern int lslib_soi968 (lsdev *hDev,unsigned short addr, unsigned short reg, unsigned char *buf, int write);

QProgressDialog * mw::progressDialog = nullptr;

mw::mw(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::mw),
    current_dev(NULL),
    hcam(NULL)
{

    ui->setupUi(this);



    lslib_init();
    connect(&get_fr, SIGNAL(timeout()), this, SLOT(get_fr_update()));
    get_fr.setSingleShot(true);

    frame_n = 0;
    err_frame_n = 0;

    connect(ui->gain_sl, SIGNAL(valueChanged(int)), this, SLOT(gain_sl_sliderMoved(int)));
    connect(ui->top_l_sl, SIGNAL(valueChanged(int)), this, SLOT(top_l_sl_sliderMoved(int)));
    connect(ui->bot_l_sl, SIGNAL(valueChanged(int)), this, SLOT(bot_l_sl_sliderMoved(int)));
    connect(ui->offset_sl, SIGNAL(valueChanged(int)), this, SLOT(offset_sl_sliderMoved(int)));
    new QShortcut(QKeySequence("ctrl+q"),this,SLOT(mwexit()));

    ui->mt_i2c_sens_addr_eb->setText("0x1003");
//    ui->hist_plot->setNotAntialiasedElements(QCP::aeAll);
//    ui->hist_plot->addGraph();
//    ui->hist_plot->xAxis->setRange(0,255);
//    ui->hist_plot->yAxis->setRange(0, 1000);
//    ui->hist_plot->setInteraction(QCP::iRangeDrag, true);
//    ui->hist_plot->setInteraction(QCP::iRangeZoom, true);
//    ui->hist_plot->setInteraction(QCP::iSelectOther, true);
//    ui->hist_plot->setAntialiasedElement(QCP::aeNone, true);
//    ui->hist_plot->setNoAntialiasingOnDrag(true);
//    QCPGraph *graph = ui->hist_plot->addGraph(ui->hist_plot->xAxis, ui->hist_plot->yAxis);
//    ui->hist_plot->xAxis->setVisible(false);
//    ui->hist_plot->yAxis->setVisible(false);
//    graph->setPen(QPen(Qt::blue));
//    ui->hist_plot->replot();
//    connect(ui->hist_plot, SIGNAL(mouseDoubleClick(QMouseEvent*)), ui->hist_plot, SLOT(rescaleAxes()));
    get_fr.start(5000);
    usb_update();
//    usb_list_timer.start(1000);
}


mw::~mw()
{
    stop_camera_capture();
    lslib_close();
    delete ui;
}

void mw::usb_update()
{
    ui->usblist->clear();

    memset(dev_tree, 0, sizeof(dev_tree));

    int dev_num = lslib_GetDeviceTree(dev_tree);

    for (int i = 0; i < dev_num; i++) {
        if (dev_tree[i].h)
        {
            ui->usblist->addItem(QString("0x") + QString::number(dev_tree[i].pid, 16)+":"+QString("0x") + QString::number(dev_tree[i].vid, 16) + " " + QString(dev_tree[i].device_name));

        }
    }

    if (ui->usblist->count())
    {
        ui->usblist->setCurrentRow(0);
    }


    return;
}
void mw::mwexit()
{
    exit(0);
}


void mw::get_fr_update()
{     

    if (!current_dev || !hcam)
    {
        // if we are already closed
        get_fr.start(10);
        return;
    }

    QList <cam::fr> *p = hcam->get_image_list();

//    qDebug() << "QL " << p->size();
    if (p->size() > 1)
    {
        struct cam::fr buf = p->front();

        if (buf.buf != NULL)
        {
            memcpy(disp_mem, buf.buf, current_dev->frame.size + current_dev->frame.embedded_data_begin_size);
            delete buf.buf;
        }
        p->pop_front();
        on_Getframe_released();


        if (p->size() > 20)
        {
            while (p->size()) {
                struct cam::fr buf = p->front();
                delete buf.buf;
                p->pop_front();
            }
        }
    }

    get_fr.start(1);
}



void mw::gain_sl_sliderMoved(int position)
{
    if (!current_dev)
        return;
    current_dev->contrast = position & 0x7F;
    if (current_dev->type == LS_A1)
    {
        libls_mt9m001(current_dev, 0x305e, (quint8 *)&current_dev->contrast, 1);
    } else {
        lslib_control_contrast(current_dev, LSLIB_WRITE);
    }
    ui->gain_txt->setText(QString("GAIN: %1").arg(position));
}

void mw::offset_sl_sliderMoved(int position)
{
    if (!current_dev)
        return;
    current_dev->bright.ee =
    current_dev->bright.eo =
    current_dev->bright.oe =
    current_dev->bright.oo = position;
    lslib_control_brightness(current_dev , LSLIB_WRITE);

    ui->offset_txt->setText(QString("OFFSET: %1").arg(position));
}
quint16 tim = 0;
void mw::on_Getframe_released()
{
    if (!current_dev)
        return;

    int res = 1;
    quint32 frame_size = current_dev->frame.size;

    QObject* obj = sender();
bool ok = false;
    tim = ui->sens_tim_eb->text().toInt(&ok, 10);
    if (obj == ui->Getframe) {
        QTime myTimer;
        myTimer.start();
        res = lslib_getImage(current_dev,disp_mem);

    }

    if (res == 0) {
        ui->fr_rate_text->setText("res: " + QString::number(res));        
    }
    else{
        qreal fps = 1000.f/(qreal)hcam->fr_speed;
        ui->fr_rate_text->setText(QString::number(hcam->fr_speed) + "ms:" + QString::number(fps,'f',1) + "/" + QString::number(fps*current_dev->frame.size/(1024*1024)));
    }

    quint8 *disp_ptr = &disp_mem[current_dev->frame.embedded_data_begin_size];

    if (ui->inverse_cb->isChecked())
        for (int x = 0; x < frame_size; x++)
                disp_ptr[x] = 255-disp_ptr[x];


    frame_image = QImage((unsigned char *) disp_ptr, current_dev->frame.width, current_dev->frame.height, QImage::Format_Grayscale8).mirrored(ui->flip_h_cb->isChecked(), ui->flip_v_cb->isChecked());

    int display_width = ui->scrollArea->height();
    if (ui->zoom_cb->isChecked())
        ui->glframe->display(frame_image.scaledToWidth(ui->zoom_sl->value()*current_dev->frame.width));
    else
        ui->glframe->display(frame_image.scaledToWidth(display_width));

    ui->pix_lt_lbl->setText(QString("%1").arg(disp_ptr[0]));
    ui->pix_rt_lbl->setText(QString("%1").arg(disp_ptr[1]));
    ui->pix_lb_lbl->setText(QString("%1").arg(disp_ptr[current_dev->frame.width]));
    ui->pix_rb_lbl->setText(QString("%1").arg(disp_ptr[current_dev->frame.width + 1]));

//    ui->frame->setPixmap( QPixmap::fromImage(img).scaled(ui->scrollArea->width()-30, ui->scrollArea->height()-30,Qt::KeepAspectRatio, Qt::SmoothTransformation) );

//    ui->frame->repaint();

   /* for(int i = 0;i< ui->hist_plot->graphCount();i++)
    {
        ui->hist_plot->graph(i)->data()->clear();
    }


    QCPGraph *g = ui->hist_plot->graph(0);
// TODO область построение гистограммы
    for (quint32 i = 0; i < frame_size; i++)
    {
        hist[disp_mem[i]]++;
    }

    for (quint32 i = 0; i < 255; i++)
    {
        g->addData(i, hist[i]);
        hist[i] = 0;
    }

     ui->hist_plot->replot(QCustomPlot::rpQueuedReplot);
     */
}

void mw::on_openclose_bt_released()
{
    int dev_number = ui->usblist->currentIndex().row();

    if (dev_number < 0)
        dev_number = 0;

    if (current_dev == NULL)
    {
        current_dev = new lsdev;
        memset(current_dev, 0, sizeof(lsdev));

        if (dev_number >= 0)
        {
            if (lslib_device_open(dev_tree[dev_number].dev_num, current_dev))
            {
                QMessageBox::critical(this, "Error", "Cannop open device", QMessageBox::Ok);
//                ui->centralWidget->setEnabled(false);
                ui->cam_control_gr->setEnabled(false);
                return;
            }
            if (current_dev->pid != 0x8613) {
                ui->bot_l_sl->setValue(current_dev->bckl.bot_left_bl);
                ui->bot_l_txt->setText(QString("Bot L Light: %1").arg(current_dev->bckl.bot_left_bl));
                ui->bot_r_sl->setValue(current_dev->bckl.bot_right_bl);
                ui->bot_r_txt->setText(QString("Bot R Light: %1").arg(current_dev->bckl.bot_right_bl));
                ui->top_l_sl->setValue(current_dev->bckl.top_left_bl);
                ui->top_l_txt->setText(QString("Top L Light: %1").arg(current_dev->bckl.top_left_bl));
                ui->top_r_sl->setValue(current_dev->bckl.top_right_bl);
                ui->top_r_txt->setText(QString("Top R Light: %1").arg(current_dev->bckl.top_right_bl));
                ui->gain_sl->setValue(current_dev->contrast);
                ui->offset_sl->setValue(current_dev->bright.ee);

                ui->dev_type_txt->setText(QString(current_dev->device_name) + QString(" V%1").arg(current_dev->version));

            }
        } else  {
            QMessageBox::critical(this, "Error", "No device with VID PID 0x04b4 0x8001", QMessageBox::Ok);
            return;
           // ui->centralWidget->setEnabled(false);
        }
        ui->openclose_bt->setText("Close Dev");
        ui->cam_control_gr->setEnabled(true);

        if (current_dev->pid != 0x8613) {
            run_camera_capture();
        }


    } else {
        ui->en_vd_cb->setChecked(false);
        QApplication::processEvents();
        if (hcam)
        {
            stop_camera_capture();
        }
         ui->cam_control_gr->setEnabled(false);
        lslib_device_close(current_dev);
        current_dev->h = NULL;
        delete current_dev;
        current_dev = NULL;
        dev_num = 0;
        ui->openclose_bt->setText("Open Dev");
    }
}

void mw::run_camera_capture()
{
    hcam = new cam(current_dev);

    hcam->start();

    hcam->setPriority(QThread::TimeCriticalPriority);

    qDebug() << "Cam thread priority " << hcam->priority();
}

void mw::stop_camera_capture()
{
    if (hcam) {
        hcam->stop();
        qDebug() << "Waiting cam_thr";
        hcam->wait(5000);
        qDebug() << "Deleting cam_thr";
        delete hcam;
        hcam = NULL;
    }
}

void mw::on_test_rom_bt_released()
{
    if (!current_dev)
        return;
#define TB_SZ 512
    int res = 0;
    quint8 buf[TB_SZ];
    for (int i = 0; i < TB_SZ; i++) buf[i] = i;
    res = lslib_SetROM(current_dev, buf, TB_SZ);
    memset(buf, 0, TB_SZ);
    res = lslib_GetROM(current_dev, buf, TB_SZ);

    //check
    for (int i = 0; i < TB_SZ; i++) {
        if (buf[i] != (i&0xFF)) {
            QMessageBox::critical(this, "Error", "Test failed", QMessageBox::Ok);
            return;
        }
    };

    QMessageBox::information(this, "Success", "Test passed", QMessageBox::Ok);
}

void mw::on_fw_read_bt_released()
{
    if (!current_dev)
        return;

    int res = lslib_control_framesize(current_dev, LSLIB_READ);

    if (res)
    {
        printf("frame read error %d\r\n", res);
        return;
    }

    lsframe * fr = &current_dev->frame;

    ui->fw_left_eb->setText(QString::number(fr->left, 10));
    ui->fw_top_eb->setText(QString::number(fr->top, 10));
    ui->fw_height_eb->setText(QString::number(fr->height, 10));
    ui->fw_width_eb->setText(QString::number(fr->width, 10));
}

void mw::on_fw_write_bt_released()
{
    if (!current_dev)
        return;

    current_dev->frame.top = ui->fw_top_eb->text().toInt();
    current_dev->frame.left = ui->fw_left_eb->text().toInt();
    current_dev->frame.height = ui->fw_height_eb->text().toInt();
    current_dev->frame.width = ui->fw_width_eb->text().toInt();
    int res = lslib_control_framesize(current_dev, LSLIB_WRITE);
}

void mw::bot_l_sl_sliderMoved(int position)
{
    if (!current_dev)
        return;
    current_dev->bckl.bot_left_bl = position & 0xff;
    lslib_control_backlight(current_dev, LSLIB_WRITE);
    ui->bot_l_txt->setText(QString("Bot L Light: %1").arg(position));
}

void mw::top_l_sl_sliderMoved(int position)
{
    if (!current_dev)
        return;
    current_dev->bckl.top_left_bl = position & 0xff;
    lslib_control_backlight(current_dev, LSLIB_WRITE);
    ui->top_l_txt->setText(QString("Top L Light: %1").arg(position));
}

void mw::on_bot_r_sl_sliderMoved(int position)
{
    if (!current_dev)
        return;
    current_dev->bckl.bot_right_bl = position & 0xff;
    lslib_control_backlight(current_dev, LSLIB_WRITE);
    ui->bot_r_txt->setText(QString("Bot R Light: %1").arg(position));
}

void mw::on_top_r_sl_sliderMoved(int position)
{
    if (!current_dev)
        return;
    current_dev->bckl.top_right_bl = position & 0xff;
    lslib_control_backlight(current_dev, LSLIB_WRITE);
    ui->top_r_txt->setText(QString("Top R Light: %1").arg(position));
}

void mw::on_expo_sl_sliderMoved(int position)
{
    if (!current_dev)
        return;

    current_dev->bright.setAverage(position);
    lslib_control_brightness(current_dev, LSLIB_WRITE);

    ui->expo_txt->setText(QString("Expo: %1").arg(position));
}

void mw::on_usblist_refresh_bt_released()
{
    usb_update();
}

void mw::on_i2c_read_bt_released()
{
    if (!current_dev)
        return;
    unsigned char data = 0;
    bool ok = false;
    lslib_soi968(current_dev, ui->i2c_addr_eb->text().toInt(&ok, 16), &data, 0);
    ui->i2c_data_eb->setText(QString::number(data,16));
}

void mw::on_i2c_write_bt_released()
{
    if (!current_dev)
        return;
    bool ok = false;
    unsigned short data = ui->i2c_data_eb->text().toInt(&ok, 16) << 8;
    lslib_soi968(current_dev, ui->i2c_addr_eb->text().toInt(&ok, 16), (unsigned char *)&data, 1);
}

struct i2c_reg_u8 {
    quint8 reg;
    quint8 val;
};

static const struct i2c_reg_u8 soi968_init[] = {
    {0x12, 0x80}, {0x0c, 0x00}, {0x0f, 0x1f},
    {0x11, 0x80}, {0x38, 0x52}, {0x1e, 0x00},
    {0x33, 0x08}, {0x35, 0x8c}, {0x36, 0x0c},
    {0x37, 0x04}, {0x45, 0x04}, {0x47, 0xff},
    {0x3e, 0x00}, {0x3f, 0x00}, {0x3b, 0x20},
    {0x3a, 0x96}, {0x3d, 0x0a}, {0x14, 0x8e},
    {0x13, 0x8b}, {0x17, 0x13}, {0x15, 0x02},
    {0x18, 0x63}, {0x19, 0x01}, {0x1a, 0x79},
    {0x32, 0x24}, {0x03, 0x00}, {0x11, 0x40},
    {0x2a, 0x10}, {0x2b, 0xe0}, {0x10, 0x32},
    {0x00, 0x00}, {0x01, 0x80}, {0x02, 0x80},
};
void mw::on_soi_init_bt_released()
{
    if (!current_dev)
        return;
    bool ok = false;
    quint32 arr_size = (sizeof(soi968_init)/sizeof(i2c_reg_u8));
    for (quint32 i = 0; i < arr_size; i++)
    {
        unsigned short data = soi968_init[i].val << 8;
        if (lslib_soi968(current_dev, soi968_init[i].reg, (unsigned char *)&data, 1) != 2)
        {
            qDebug() << "I2C error";
        }
    }
}

void mw::setPercentOfComplete(int progress) {
    QApplication::processEvents();
    if (mw::progressDialog != nullptr)
        progressDialog->setValue(progress);
}

void mw::on_wrom_bt_released()
{
    if (!current_dev)
        return;

    ui->centralWidget->setEnabled(false);

#define SIZELIMIT 0x1000

    ui->en_vd_cb->setChecked(false);

    QString ROMfileName = QFileDialog::getOpenFileName(this, ("Open File"),
                                                    "",
                                                    ("ROM Image (*.iic)"), 0, QFileDialog::DontUseNativeDialog);


    QFile pf(ROMfileName);
    if (!pf.open(QIODevice::ReadOnly)) {
        qDebug() << "File open error";
        ui->centralWidget->setEnabled(true);
        QMessageBox::information(this, "Error","File open error");
        return;
    }

    QByteArray data = pf.read(pf.size());
    progressDialog = new QProgressDialog(this);
    progressDialog->setLabelText("Writing to FX2 ROM...");
    QProgressBar pb;
    pb.setFormat("%v of %m");
    progressDialog->setBar(&pb);
    progressDialog->setCancelButton(nullptr);
    progressDialog->show();

    int res = lslib_WriteFirmware(current_dev, (unsigned char *)data.data(), pf.size(), this->setPercentOfComplete);

    progressDialog->deleteLater();

    pf.close();

    ui->centralWidget->setEnabled(true);
    if (res == LSLIB_NO_ERROR)
        QMessageBox::information(this, "ROM write","Complete");
    else if (res == LSLIB_ROM_SIZE_OVF) {
        QMessageBox::information(this, "Error","Too big file. Flash is only 8K");
    } else
        QMessageBox::information(this, "ROM write",QString("Failed %1").arg(res));
}

void mw::on_clear_rom_bt_released()
{
    if (!current_dev)
        return;
    unsigned char p[0x1e00];
    ui->en_vd_cb->setChecked(false);
    memset(p, 0, sizeof(p));
    int res = lslib_WriteFirmware(current_dev, p, sizeof(p), nullptr);
}


#include <fxload/usb.h>
extern "C"
{
int ezusb_load_ram (libusb_device_handle *device, const char *path, int fx2, int stage);
void logerror(const char *format, ...)
    __attribute__ ((format (__printf__, 1, 2)));
void logerror(const char *format, ...)
{
    va_list ap;
    va_start(ap, format);

    vfprintf(stderr, format, ap);
    va_end(ap);
}
}


extern int verbose;
void mw::on_wram_bt_released()
{
    if (!current_dev)
        return;

    QString RAMfileName = QFileDialog::getOpenFileName(this, ("Open File"),
                                                    "",
                                                    ("RAM Image (*.hex)"), 0, QFileDialog::DontUseNativeDialog);

    lslib_download_fx2(current_dev, RAMfileName.toLatin1().data());
    //qDebug() << "LOAD RAM RSEULT " << lslib_downloadFirmware( current_dev, RAMfileName.toLatin1().data());
//    if (ezusb_load_ram ((libusb_device_handle *)current_dev->h, RAMfileName.toLatin1().data(), 1, 0) == 0)
//        qDebug() << "LOAD RAM RESULT" << ezusb_load_ram ((libusb_device_handle *)current_dev->h, RAMfileName.toLatin1().data(), 1, 0);
}

void mw::on_mt_i2c_read_bt_released()
{
    if (!current_dev)
        return;
    unsigned char data[2];
    bool ok = false;

//    if (current_dev->type < LS_A4)
        libls_mt9m001(current_dev, ui->mt_i2c_addr_eb->text().toInt(&ok, 16), (unsigned char *)&data, 0);
//    else
//        libls_sensor(current_dev, ui->mt_i2c_sens_addr_eb->text().toInt(&ok, 16), ui->mt_i2c_addr_eb->text().toInt(&ok, 16), (unsigned char *)&data, 0);

    unsigned short rd = *(quint16 *)data;
    ui->mt_i2c_data_eb->setText(QString::number(rd,16));
}

void mw::on_mt_i2c_write_bt_released()
{
    if (!current_dev)
        return;
    bool ok = false;
    unsigned short wr = ui->mt_i2c_data_eb->text().toInt(&ok, 16);
//    if (current_dev->type < LS_A4)
        libls_mt9m001(current_dev, ui->mt_i2c_addr_eb->text().toInt(&ok, 16), (unsigned char *)&wr, 1);
//    else
//        libls_sensor(current_dev, ui->mt_i2c_sens_addr_eb->text().toInt(&ok, 16), ui->mt_i2c_addr_eb->text().toInt(&ok, 16), (unsigned char *)&wr, 1);
}



void mw::on_en_vd_cb_toggled(bool checked)
{
    if (hcam)
        hcam->set_capture(checked?1:0);
}

void mw::on_trig_cb_stateChanged(int arg1)
{
    if (!current_dev)
        return;
    quint8 data[2] = {0,0};
    quint16 len = 2;
    lslib_vendor_req(current_dev, 0xd2, arg1, 0, data, len);
}



void mw::on_save_img_bt_released()
{
    try {
        frame_image.toPixelFormat(QImage::Format_Indexed8);
        frame_image.save("image.png");
    } catch (...) {

    }
}

void mw::on_en_autoexp_cb_toggled(bool checked)
{
    if (!current_dev)
        return;

    quint16 buf = checked?1:0;
    libls_mt9m001(current_dev, 0x3100, (quint8 *)&buf, 1);
}

void mw::on_ac_fw_width_eb_textChanged(const QString &arg1)
{
    current_dev->frame.width = ui->ac_fw_width_eb->text().toUInt();
}

void mw::on_extra_data_eb_textChanged(const QString &arg1)
{
    current_dev->frame.embedded_data_begin_size = ui->extra_data_eb->text().toUInt();
}

void mw::on_rst_sens_bt_released()
{
    if (!current_dev)
        return;
    if (lslib_a1SensorReset(current_dev) == LSLIB_NO_ERROR) {
        lslib_a1FX2Reset(current_dev);
    } else qDebug() << "RESET Failed";
}


void mw::on_test_rom_bt_2_released()
{

}


void mw::on_zoom_cb_toggled(bool checked)
{
    ui->zoom_sl->setEnabled(checked);
}


void mw::on_zoom_sl_valueChanged(int value)
{
    ui->glframe->display(frame_image.scaledToWidth(value*current_dev->frame.width));
}

