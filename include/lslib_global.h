#ifndef LSLIB_GLOBAL_H
#define LSLIB_GLOBAL_H

#define LSLIB_MAXDEVICES        10    /*! Максимально количество устройств, подсоединяемых к либе */
#define LSLIB_NO_ERROR           0
#define LSLIB_SENS_ERROR        -1
#define LSLIB_I2C_ERROR         -2
#define LSLIB_NO_DEVICE         -3
#define LSLIB_ROM_SIZE_OVF      -4
#define LSLIB_ROM_WRITE_ERR     -5

// Структура типов устройства
typedef enum _device_type {
    NOFLEX,
    LS7,		// анаморфотный прокатный, дроздов
    LS2FP,		// двупалый, дроздов
    LS1FP,		// однопалый, дроздов
    LSPP,		// роликовый
    LSPPFP,		// роликовый с прокаткой
    LSPFP,		// плоская призма
    LS1FP10,    // 10 бит. Порядок [старшие 2][младшие 8]
    LS2FP10,
    LS1FP_M,     // 8 или 10 бит. Порядок [младшие 2][старшие 8],
    LS8FP,       // 8 бит [старшие 8]
    LS_A1,
    LS_A2,
    LS_A4
}device_type, *pdevice_type;

enum operation {
    LSLIB_READ = 0,
    LSLIB_WRITE
};

typedef void * lslib_handle;

// структура параметров изображения
typedef struct _lsframe {
    unsigned int top;
    unsigned int left;
    unsigned int height;
    unsigned int width;
    unsigned int size;
    unsigned int embedded_data_begin_size;  // размер вложенных данных в начале кадра
    unsigned int embedded_data_end_size;    // размер вложенных данных в конце кадра
} lsframe;

// структура значений яркости в матрице
typedef struct _lsbrightness {
    short ee; // Even Row, Even Column
    short oe; // Odd Row, Even Column
    short eo; // Even Row, Odd Column
    short oo; // Odd Row, Odd Column
    short average() {return ee+oe+eo+oo/4.;}
    void setAverage(short val) {
        ee = oe = eo = oo = val;
    }
} lsbright;

// структура значений подсветок в сканере
typedef struct _lsbacklight {
    unsigned char bot_right_bl; // нижняя подсветка правой камеры
    unsigned char top_right_bl; // верхняя подсветка правой камеры
    unsigned char bot_left_bl;  // нижняя подсветка левой камеры
    unsigned char top_left_bl;  // верхняя подсветка левой камеры
} lsbacklight;

typedef struct {
    int dev_num;                // порядковый номер в списке устройств
    lslib_handle *h;            // указатель на устройство, используется
                                // внутри библиотеки
    unsigned short vid;         /* Vendor ID */
    unsigned short pid;         /* Product ID */
    unsigned char is_open;      /* When device is opened, val = 1 */
    unsigned char busnum;       /* The bus number of this device */
    unsigned char devaddr;      /* The device address*/
    char device_name[8];        // Имя устройства _device_type в строчке
    unsigned char version;      // flex version
    lsframe frame;              // геометрические параметры передаваемого
                                // изображения
    unsigned int serial;        // серийный номер
    unsigned char contrast;     // Текущий код контраста на матрице 0-127
    lsbright bright;            // Текущий код яркости на матрице
    lsbacklight bckl;           // Текущий код яркости подсветок в сканере
    unsigned int coarseExpo;
    unsigned int fineExpo;
    device_type type;
} lsdev;

/* Function prototypes */
/*!
 * \brief lslib_init - ОБЯЗАТЕЛЬНО ВЫЗВАТЬ В САМОМ НАЧАЛЕ РАБОТЫ
 * \return
 */
extern int lslib_init();

/*!
 * \brief lslib_close - закрывает все устройства, libusb.
 * Выполнять только при завершении приложения.
 */
extern void lslib_close();

/*!
 * \brief lslib_error - получение строки с описанием кода ошибки
 * \param err - код ошибки
 */
extern char * lslib_error(int err);

/*!
 * \brief lslib_open - открыть устройство
 * \return
 */
extern int lslib_device_open(int dev_num, lsdev *);

/*!
 * \brief lslib_device_close - закрыть устройство
 * \param h
 */
extern void lslib_device_close(lsdev *h);

/*!
 * \brief lslib_control_framesize
 * \param dev
 * \param rw
 * \return
 */
extern int lslib_control_framesize (lsdev * dev, operation rw);

/*!
 * \brief lslib_control_backlight
 * \param dev
 * \param rw
 * \return
 */
extern int lslib_control_backlight (lsdev * dev, operation rw);

/*!
 * \brief lslib_set_contrast - управление контрастом
 * \param h - указатель на устройство
  * \param value - значение от 0 до 63
 * \return
 */
extern int lslib_control_contrast (lsdev * dev, operation rw);
/*!
 * \brief lslib_control_brightness
 * \param dev
 * \param rw
 * \return
 */
extern int lslib_control_brightness (lsdev * dev, operation rw);

/*!
 * \brief lslib_getImage - получить изображение
 * \param h - указатель на устройство
 * \param buff - указатель на массив не менее FRAME_SZ
 * \return - возвращает кол-во принятых байт (в зависимости от типа сканера!)
 */
extern int lslib_getImage(lsdev * dev, unsigned char *buff);
extern int lslib_flow(lsdev *dev, unsigned char *buff);
/*!
 * \brief lslib_GetDeviceTree - получить дерево устройств LS для подключения
 * \param dt
 * \return number of devices
 */
extern int lslib_GetDeviceTree( lsdev *dt );

/*!
 * \brief lslib_GetROM
 * \param hDev
 * \param pData
 * \param len
 * \return
 */
extern int lslib_GetROM(lsdev *hDev, unsigned char * pData, unsigned  int len);
/*!
 * \brief lslib_SetROM
 * \param hDev
 * \param pData
 * \param len
 * \return
 */
extern int lslib_SetROM(lsdev *hDev, unsigned char * pData, unsigned  int len);
/*!
 * \brief lslib_WriteFirmware
 * \param hDev
 * \param pData
 * \param len
 * \return
 */
//extern int lslib_WriteFirmware(lsdev *hDev, unsigned char * pData, unsigned  int len);
extern int lslib_WriteFirmware(lsdev *hDev, unsigned char * pData, unsigned  int len, void (*f)(int));
extern int lslib_soi968 (lsdev *hDev, unsigned short reg, unsigned char *buf, int write);

extern int libls_mt9m001 (lsdev *hDev, unsigned short reg, unsigned char *buf, int write);
extern int libls_sensor (lsdev *hDev, unsigned short sens_addr, unsigned short reg, unsigned char *buf, int write);
extern int lslib_vendor_req(lsdev *dev, unsigned char req, unsigned short value, unsigned short index, unsigned char *data, unsigned short len);

extern int lslib_flowctl(lsdev *hDev, int start);

extern int lslib_a1SensorReset(lsdev *dev);
extern int lslib_a1FX2Reset(lsdev *dev);
extern int lslib_download_fx2(lsdev *h, char *filename);

#endif // LSLIB_GLOBAL_H
