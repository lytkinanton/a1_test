#-------------------------------------------------
#
# Project created by QtCreator 2015-05-16T13:21:03
#
#-------------------------------------------------

QT       += core gui

CONFIG += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lsfp_ta
TEMPLATE = app
 
win32 {
    LIBS += $$PWD/libusb/64/libusb-1.0.a #$$PWD/libusb/64/libusb.a
}
unix {
    LIBS += -lusb-1.0
}

INCLUDEPATH += libusb
 

SOURCES += main.cpp \
   # cyapi.cpp \
    framer.cpp \
        mw.cpp \
    ls.cpp \
    fxload/ezusb.c

HEADERS  += mw.h \
    CyUSB30_def.h \
    UsbdStatus.h \
    VersionNo.h \ 
    cyioctl.h \
    framer.h \
    include/lslib_global.h \
    cam.h \
    ls.h \
    fxload/ezusb.h \
    fxload/usb.h \
    usb100.h \
    usb200.h

FORMS    += mw.ui


