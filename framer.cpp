#include "framer.h"

//framer::framer(QWidget *parent) : QOpenGLWidget(parent)


void framer::display(const QImage& img)
{
//  image = img;

//  this->update();

    QMatrix matrix;
    QPoint center = img.rect().center();
    matrix.translate(center.x(), center.y());
    matrix.rotate(270);
    QImage dstImg = img.transformed(matrix);
    QPixmap dstPix = QPixmap::fromImage(dstImg);
    setPixmap(dstPix);
}

//void framer::paintGL()
//{
//    QPainter painter(this);
//    painter.drawImage(this->rect(),image);
//}
